"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isSubtree(dom, vdom) {
    return stringFromPreOrder(dom).indexOf(stringFromPreOrder(vdom)) > -1;
}
exports.isSubtree = isSubtree;
function stringFromPreOrder(tree) {
    if (!tree) {
        return "";
    }
    return tree.value + stringFromPreOrder(tree.left) + stringFromPreOrder(tree.right);
}
//# sourceMappingURL=original.js.map