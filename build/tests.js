"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const original_1 = require("./original");
const fix_1 = require("./fix");
const kary_1 = require("./kary");
const backtracking_1 = require("./backtracking");
const merkletree_1 = require("./merkletree");
const jsdom_1 = require("jsdom");
const fs = require("fs");
let basetest = {
    name: "Original test",
    description: "Test passes with original solution",
    iskary: false,
    expected: true,
    dom: {
        value: "root",
        left: {
            value: "a",
            left: {
                value: "c",
                left: {
                    value: "g"
                },
                right: {
                    value: "h"
                }
            },
            right: {
                value: "d",
                left: {
                    value: "i"
                }
            }
        },
        right: {
            value: "b",
            left: {
                value: "e",
                right: {
                    value: "j",
                    left: {
                        value: "k"
                    },
                    right: {
                        value: "l"
                    }
                }
            },
            right: {
                value: "f"
            }
        }
    },
    vdom: {
        value: "a",
        left: {
            value: "c",
            left: {
                value: "g"
            },
            right: {
                value: "h"
            }
        },
        right: {
            value: "d",
            left: {
                value: "i"
            }
        }
    }
};
let regressiontest = {
    name: "Regression test",
    description: "Breaks original solution. A subtree of dom and vdom have different structure but produce same traversal string.",
    expected: false,
    iskary: false,
    dom: {
        value: 'a',
        left: {
            value: 'b',
            left: {
                value: 'c',
                left: {
                    value: 'd'
                },
                right: {
                    value: 'e'
                }
            },
            right: {
                value: 'f'
            }
        }
    },
    vdom: {
        value: 'b',
        left: {
            value: 'c'
        },
        right: {
            value: 'd',
            left: {
                value: 'e'
            }
        }
    }
};
let sotest = {
    name: "Web page test (k-ary)",
    description: "Html page",
    iskary: true,
    expected: true,
    dom: jsdom_1.JSDOM.fragment(fs.readFileSync("so.html").toString()),
    vdom: jsdom_1.JSDOM.fragment(fs.readFileSync("so_subtree.html").toString()).firstChild
};
console.log("dom: " + sotest.dom);
console.log("vdom: " + sotest.vdom);
let sotest2 = {
    name: "Web page test 2 (k-ary)",
    description: "Html page",
    iskary: true,
    expected: true,
    dom: jsdom_1.JSDOM.fragment(fs.readFileSync("so.html").toString()),
    vdom: jsdom_1.JSDOM.fragment(fs.readFileSync("so_subtree2.html").toString()).firstChild
};
let sotest3 = {
    name: "Negative - Web page test 3 (k-ary)",
    description: "Html page",
    iskary: true,
    expected: false,
    dom: jsdom_1.JSDOM.fragment(fs.readFileSync("so.html").toString()),
    vdom: jsdom_1.JSDOM.fragment(fs.readFileSync("not_subtree.html").toString()).firstChild
};
let allTests = [basetest, regressiontest, sotest, sotest2, sotest3];
// run Tests
allTests.forEach(test => {
    console.log("\n=================================================\n--------------------------------\nTest name: " + test.name);
    console.log(test.description + "\n-------------------------------");
    console.log("Expected: " + test.expected);
    if (!test.iskary) {
        console.log("Original solution: " + original_1.isSubtree(test.dom, test.vdom) + "\n");
        console.log("Fixed solution: " + fix_1.isSubtreeFixed(test.dom, test.vdom) + "\n");
    }
    else {
        console.time("String comp");
        console.log("K-ary string comparison solution: " + kary_1.isSubtreeKary(test.dom, test.vdom) + "\n");
        console.timeEnd("String comp");
        console.time("Backtracking");
        console.log("K-ary backtracking solution: " + backtracking_1.isSubtreeBacktrackingSolution(test.dom, test.vdom) + "\n");
        console.timeEnd("Backtracking");
        console.time("Merkle Tree");
        console.log("K-ary merkle tree solution: " + merkletree_1.isSubtreeMerkleTreeSolution(test.dom, test.vdom) + "\n");
        console.timeEnd("Merkle Tree");
    }
});
//# sourceMappingURL=tests.js.map