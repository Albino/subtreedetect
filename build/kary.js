"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isSubtreeKary(dom, vdom) {
    return stringFromTree(dom).indexOf(stringFromTree(vdom)) > -1;
}
exports.isSubtreeKary = isSubtreeKary;
function stringFromTree(tree, depth = 0) {
    let res = "<" + tree.nodeName + ">";
    if (tree.hasChildNodes) {
        tree.childNodes.forEach(child => {
            res += stringFromTree(child, depth + 1);
        });
    }
    res += "</" + tree.nodeName + ">";
    return res;
}
//# sourceMappingURL=kary.js.map