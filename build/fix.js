"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// fix the issue in the original algorithm where the tree structure wasn't being taken into consideration.
function isSubtreeFixed(dom, vdom) {
    return stringFromPreOrderFixed(dom).indexOf(stringFromPreOrderFixed(vdom)) > -1;
}
exports.isSubtreeFixed = isSubtreeFixed;
function stringFromPreOrderFixed(tree) {
    if (!tree) {
        return "";
    }
    return "<" + tree.value + ">" + stringFromPreOrderFixed(tree.left) + stringFromPreOrderFixed(tree.right) + "</" + tree.value + ">";
}
//# sourceMappingURL=fix.js.map