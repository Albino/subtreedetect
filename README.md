# Report #
========

## Questions ##
---------

**This code will not work in certain scenarios. Identify the scenarios it will not work for.**

In it's original form, the algorithm can easily generate the same string from trees with different structures.
For example, it fails for the following pair DOM, VDOM:
![DOM VDOM sample](diagrams/originalfail.png)

The pre-order traversal of DOM yields "ROOTabcdefghij" whereas VDOM traversal yields abcdefg (substring), however the tree structures are different.

**What would you add or modify to this algorithm to make it work?**

One solution is to add node opening/closing tags when building the dom string. Basically, build a XML like string representation of the DOM and VDOM (_see fix.ts_ )

**Write a few test cases that show that your new code covers different scenarios.**

_See the tests.ts file._

**Now, in the real world, the DOM is a tree with an arbitrary number children on each node. How would you modify this code, which is currently for a binary tree, to make it work for a k-ary tree, where k is the maximum number of children a node can have?**
The solution is a straightforward generalization of the binary version in _fix.ts_. Basically, instead of visiting left and right we have a loop that iterate through the child nodes. (
_See the kary.ts file._)

**Constructing strings and doing string comparisons is immensely expensive, both memory-wise and computation-wise. Devise an algorithm other than comparing generated strings to determine if a k-ary tree is a subtree of another k-ary tree.**

I understand that the original algorithm has a __space__ requirement of O(n+m) where n is length of the string representation of the DOM and m is length of the string representation of the VDOM. Most likely javascript implementation of string.indexOf has __computational__ complexity O(n+m) on average and O(n\*m) worst case. Note that, we would still have to traverse all the nodes of both the DOM and VDOM at a computational cost of the order of O(N+M) string concatenations, where N/M is the number of nodes in the DOM/VDOM.

We could mitigate the problem of the expensive string concatenations by introducing a StringBuilder object that would be shared throughout the recursive tree traversal, however, this wouldn't solve the problem of O(n+m) space.

We propose two different algorithms both costing O(1) space.

The first is a backtracking algorithm that exploit the fact that the VDOM can only be a subtree if its leafs matches leafs of the DOM.
The algorithm proposed here visits the candidate DOM leafs (leafs that could match the leftmost leaf of the VDOM) and only try to match relative to those particular leafs. This step garantees that we filter out regions of the DOM that could not fit in the VDOM given our knowledge of the shape of the VDOM. See the backtracking.ts file.

The worst case computational cost of the backtracking algorithm is O(N\*M). However, the average cost is much smaller. As it is usually the case with backtracking algorithms, estimating the avergate cost is not trivial as it depends on how the input data looks like on average. On a regular web page where there is lots of variation among the parts the traversal-match routine will cost O(logM) on average yielding a O(N\*logM) overall cost.

The second proposal is based on the hash code scheme of [Merkle Trees](https://en.wikipedia.org/wiki/Merkle_tree). On a Merkle Tree every node keeps a hash code computed from the hash code of its children. In my case, I compute the hash codes for every node but I don't store them. They are used and discarded, thus, space requirements of the algorithm is not dependent of the size of the data structure (O(1)).

The computational cost of the this algorithm is O(N+M).

On the test implemented in _tests.ts_, the merkle tree based algorithm perfomed consistently better (see _runlogs1_ and _runlogs2_). However, both the backtracking algorithm and he merkle tree algorithm are clearly superior to the original string building algorithm which ratifies the analysis above.