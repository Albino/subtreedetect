import { IBinaryNode } from "./domnode";

// fix the issue in the original algorithm where the tree structure wasn't being taken into consideration.
export function isSubtreeFixed (dom: IBinaryNode, vdom: IBinaryNode): boolean {
    return stringFromPreOrderFixed(dom).indexOf(stringFromPreOrderFixed(vdom)) > -1;
}

function stringFromPreOrderFixed(tree: IBinaryNode): string {
    if (!tree) {
        return "";
    }
    return "<"+tree.value+">" + stringFromPreOrderFixed(tree.left) + stringFromPreOrderFixed(tree.right) + "</" + tree.value + ">";
}