import {IBinaryNode} from "./domnode";

export function isSubtree(dom: IBinaryNode, vdom: IBinaryNode): boolean {
    return stringFromPreOrder(dom).indexOf(stringFromPreOrder(vdom)) > -1;
}

function stringFromPreOrder(tree: IBinaryNode): string {
    if (!tree) {
        return "";
    }
    return tree.value + stringFromPreOrder(tree.left) + stringFromPreOrder(tree.right);
}
