export interface IBinaryNode {
    value: string;
    left?: IBinaryNode;
    right?: IBinaryNode;
}