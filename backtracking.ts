
export function isSubtreeBacktrackingSolution(dom: Node, vdom: Node): boolean {
    let minDepth: number = getLeftMostLeafDepth(vdom);
    return visitCandidateLeafs(dom, vdom, minDepth);
}

// get leftmost leaf starting from given tree node
function getLeftMostLeafDepth(tree: Node, depth: number = 0): number {
    if((!tree.childNodes || tree.childNodes.length === 0) && (tree.parentNode == null || tree.parentNode.firstChild === tree)) {
        return depth;
    }
    return getLeftMostLeafDepth(tree.firstChild, depth+1);
}

// traverse DOM finding candidate leaf nodes
function visitCandidateLeafs(tree: Node, vdom: Node, minDepth: number, depth:number = 0): boolean {
    if((depth > minDepth)
        && (!tree.childNodes || tree.childNodes.length === 0)
        && (tree.parentNode == null || tree.parentNode.childNodes[0] === tree)) {
        // we have a candidate leaf
        let candidateRoot: Node = tree;
        for(let i:number = minDepth; i > 0 ; i--) {
            candidateRoot = candidateRoot.parentNode;
        }
        return traverseMatch(candidateRoot, vdom);
    }

    for(let i:number = 0 ; i < tree.childNodes.length ; i++) {
        if( visitCandidateLeafs(tree.childNodes[i], vdom, minDepth, depth + 1) ) {
            return true;
        }
    }
    return false;
}

// traverse both the candidate subtree and the virtual DOM simultaneously while cheking if it's a match
function traverseMatch( candidate: Node, vdom: Node): boolean {
    if((candidate.nodeName !== vdom.nodeName)
        || (candidate.childNodes && vdom.childNodes && candidate.childNodes.length !== vdom.childNodes.length) ) {
            return false;
    }

    for(let i:number = 0; i < candidate.childNodes.length ; i++) {
        if(!traverseMatch(candidate.childNodes[i], vdom.childNodes[i])){
            return false;
        }
    }
    return true;
}