export function isSubtreeKary(dom: Node, vdom: Node): boolean {
    return stringFromTree(dom).indexOf(stringFromTree(vdom))> -1;
}

function stringFromTree(tree: Node, depth: number = 0): string {
    let res:string = "<"+tree.nodeName+">";
    if(tree.hasChildNodes) {
        tree.childNodes.forEach( child => {
            res += stringFromTree(child, depth + 1);
        });
    }
    res += "</"+tree.nodeName+">";
    return res;
}