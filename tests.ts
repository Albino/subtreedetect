import { isSubtree } from "./original";
import { isSubtreeFixed } from "./fix";
import { IBinaryNode } from "./domnode";
import { isSubtreeKary } from "./kary";
import { isSubtreeBacktrackingSolution } from "./backtracking";
import { isSubtreeMerkleTreeSolution } from "./merkletree";
import { JSDOM } from "jsdom";
import * as fs from "fs";

interface ITest {
    name: string;
    description: string;
    expected: boolean;
    iskary: boolean;
    dom?: IBinaryNode | Node;
    vdom?: IBinaryNode | Node;
}

let basetest: ITest = {
    name: "Original test",
    description: "Test passes with original solution",
    iskary:false,
    expected: true ,// vdom is a subtree
    dom: <IBinaryNode>{
        value: "root",
        left: {
            value: "a",
            left: {
                value: "c",
                left: {
                    value: "g"
                },
                right: {
                    value: "h"
                }
            },
            right: {
                value: "d",
                left: {
                    value: "i"
                }
            }
        },
        right: {
            value: "b",
            left: {
                value: "e",
                right: {
                    value: "j",
                    left: {
                        value: "k"
                    },
                    right: {
                        value: "l"
                    }
                }
            },
            right: {
                value: "f"
            }
        }
    },
    vdom: <IBinaryNode>{
            value: "a",
            left: {
                value: "c",
                left: {
                    value: "g"
            },
            right: {
                value: "h"
            }
        },
        right: {
            value: "d",
            left: {
                value: "i"
            }
        }
    }
};


let regressiontest: ITest = {
    name: "Regression test",
    description: "Breaks original solution. A subtree of dom and vdom have different structure but produce same traversal string.",
    expected: false,// not a subtree
    iskary: false,
    dom: <IBinaryNode> {
        value: 'a',
        left: {
            value: 'b',
            left: {
                value: 'c',
                left: {
                    value: 'd'
                } ,
                right: {
                    value: 'e'
                }
            },
            right: {
                value: 'f'
            }
        }
    },
    vdom: <IBinaryNode>{
        value: 'b',
        left: {
            value: 'c'
        },
        right: {
            value: 'd',
            left: {
                value: 'e'
            }
        }

    }
};

let sotest: ITest = {
    name: "Web page test (k-ary)",
    description: "Html page",
    iskary: true,
    expected: true,
    dom: JSDOM.fragment(fs.readFileSync("so.html").toString()),
    vdom: JSDOM.fragment(fs.readFileSync("so_subtree.html").toString()).firstChild
};

console.log("dom: "+ sotest.dom);
console.log("vdom: "+ sotest.vdom);

let sotest2: ITest = {
    name: "Web page test 2 (k-ary)",
    description: "Html page",
    iskary: true,
    expected: true,
    dom: JSDOM.fragment(fs.readFileSync("so.html").toString()),
    vdom: JSDOM.fragment(fs.readFileSync("so_subtree2.html").toString()).firstChild
};

let sotest3: ITest = {
    name: "Negative - Web page test 3 (k-ary)",
    description: "Html page",
    iskary: true,
    expected: false,
    dom: JSDOM.fragment(fs.readFileSync("so.html").toString()),
    vdom: JSDOM.fragment(fs.readFileSync("not_subtree.html").toString()).firstChild
};

let allTests: ITest[] = [basetest, regressiontest, sotest, sotest2, sotest3];

// run Tests
allTests.forEach(test => {
    console.log("\n=================================================\n--------------------------------\nTest name: " + test.name);
    console.log(test.description + "\n-------------------------------");
    console.log("Expected: " + test.expected);

    if(!test.iskary) {
        console.log("Original solution: " + isSubtree(<IBinaryNode>test.dom, <IBinaryNode>test.vdom)+"\n");
        console.log("Fixed solution: " + isSubtreeFixed(<IBinaryNode>test.dom, <IBinaryNode>test.vdom)+"\n");
    }else {
        console.time("String comp");
        console.log("K-ary string comparison solution: "+ isSubtreeKary( <Node>test.dom, <Node>test.vdom)+"\n" );
        console.timeEnd("String comp");
        console.time("Backtracking");
        console.log("K-ary backtracking solution: " + isSubtreeBacktrackingSolution(<Node>test.dom, <Node>test.vdom)+"\n");
        console.timeEnd("Backtracking");
        console.time("Merkle Tree");
        console.log("K-ary merkle tree solution: "+ isSubtreeMerkleTreeSolution(<Node>test.dom, <Node>test.vdom)+"\n");
        console.timeEnd("Merkle Tree");

    }

});