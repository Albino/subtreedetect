
interface ITraverseResult {
    isMatch: boolean;
    hashCode: number;
}

export function isSubtreeMerkleTreeSolution(dom: Node, vdom: Node): boolean {
    let vdomRootHashCode: ITraverseResult = traverseHashCodeMatch(vdom, 0, true);
    let res: ITraverseResult = traverseHashCodeMatch(dom, vdomRootHashCode.hashCode, false);
    return res.isMatch;
}

export function traverseHashCodeMatch(node: Node, vdomHashCode: number, dryrun: boolean): ITraverseResult {
    let res: ITraverseResult = {
        isMatch: false,
        hashCode:-1
    };

    let aggregatedChildrenHashes: string = "";

    if (node.hasChildNodes) {
        for(let i: number= 0 ; i < node.childNodes.length ; i++) {
            let childRes: ITraverseResult = traverseHashCodeMatch(node.childNodes[i], vdomHashCode, dryrun);
            if (!dryrun && childRes.isMatch) {
                return childRes;
            }
            aggregatedChildrenHashes += childRes.hashCode;
        }
    }else {
        aggregatedChildrenHashes = node.nodeName;
    }

    res.hashCode = computeHashCode(aggregatedChildrenHashes);
    res.isMatch = res.hashCode === vdomHashCode;

    return res;
}

function computeHashCode(str: string): number {
    let hash:number = 0;
    for (let i: number = 0; i < str.length; i++) {
        let char:number = str.charCodeAt(i);
        hash = char + (hash << 6) + (hash << 16) - hash;
    }
    return hash;
}